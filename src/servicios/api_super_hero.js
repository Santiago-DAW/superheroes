const token = "108225775495123";
const url = "https://www.superheroapi.com/api.php/";
const url_base = `${url}${token}/`;

console.log(url_base);

// Devuelve una funcion con la url base de SuperHero. :function
const buscador_API = (url_base) => {
  return (valor_busqueda) =>
    fetch(`${url_base}${valor_busqueda}`)
      .then(respuesta => respuesta.json())
};

// Funcion, realiza una busqueda por el id del personaje. :Promise - personaje
const busqueda_id_API = buscador_API(url_base);
// Funcion, realiza una busqueda por el nombre del personaje. :Promise - personaje/es
const busqueda_nombre_API = buscador_API(url_base + "search/");

export { busqueda_id_API, busqueda_nombre_API };


// ---- Estructura de peticiones Api SuperHero ---- 
// /id
// https://superheroapi.com/api/access-token/character-id
//     /powerstats
//     /biography
//     /appearance
//     /work
//     /connections
//     /image
// /search/name
// https://superheroapi.com/api/access-token/search/name
