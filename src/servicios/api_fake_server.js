const url_base = "http://localhost:4000/";
const personajes = "http://localhost:4000/personajes";
const API_ENDPOINT_USERS = "http://localhost:4000";

// Devuelve una funcion, lista para usar, con la url base y su clave de busqueda en FAKE SERVER. :function
const get_API_FAKE = (clave) => {
  return () =>
    fetch(`${url_base}${clave}`)
      .then(response => response.json());
};

// Devuelve un array con todos los usuarios almacenados en FAKE SERVER. :Promise - Array objetos usuario
const get_usuarios_API_FAKE = get_API_FAKE("usuarios");
// Devuelve un array con todos los favoritos almacenados en FAKE SERVER. :Promise - Array objetos favorito
const get_favoritos_API_FAKE = get_API_FAKE("favoritos");
// Devuelve un array con todos los personajes almacenados en FAKE SERVER. :Promise - Array objetos personje
const get_personajes_API_FAKE = get_API_FAKE("personajes");

// Devuelve los datos de una clave (usuarios, favoritos o personajes) de FAKE SERVER. :Promise - Array objetos
const get_datos_API_FAKE = (clave) => {
  return fetch(`${url_base}${clave}`)
    .then(response => response.json());
};

const getUsers = () => fetch(`${API_ENDPOINT_USERS}/usuarios`)
.then((response) => response.json());

const newUser = (userName, userPassword) => fetch(`${API_ENDPOINT_USERS}/usuarios`, {
    method: 'POST',
    headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        userName,
        userPassword,
        "favorites": []
    })
}).then(response => response.json());

const checkUser = (userName, userPassword) => fetch(`${API_ENDPOINT_USERS}/usuarios?userName=${userName}&userPassword=${userPassword}`)
.then((response) => response.json());

const userExists = (userName) => fetch(`${API_ENDPOINT_USERS}/usuarios?userName=${userName}`)
.then((response => response.json()))

const nameDuplicated = (userName) => fetch(`${API_ENDPOINT_USERS}/usuarios?userName=${userName}`)
.then((response => response.json()))

const deleteUser = (idUser) => fetch(`${API_ENDPOINT_USERS}/usuarios/${idUser}`, {
    method: 'DELETE'
})
.then(response => response.json());

const addFavorite = (idUser, favorites) => fetch(`${API_ENDPOINT_USERS}/usuarios/${idUser}`, {
    method: 'PATCH',
    headers: {
        'Content-Type': 'application/json'
    },
    body: JSON.stringify({
        "favorites": favorites
    })
}).then(response => response.json());

export { getUsers, newUser, checkUser, userExists, nameDuplicated, deleteUser, addFavorite }

export { get_usuarios_API_FAKE, get_favoritos_API_FAKE, get_personajes_API_FAKE, get_datos_API_FAKE };
