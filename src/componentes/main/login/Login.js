import { useCallback, useEffect } from "react";
import { useState } from "react";
import { Navigate, redirect } from "react-router";
import { checkUser, userExists } from "../../../servicios/api_fake_server";
import Config from "../cuenta/Config";
import Cuenta from "../cuenta/Cuenta";
import Registro from "../registro/Registro";
import "./login.css";

function Login({auth, setAuth, dataUser, setDataUser}) {
    const [sent, setSent] = useState(false);
    const [logWithError, setLogWithError] = useState(false);
    const [newUser, setNewUser] = useState(false);
    const [userName, setUserName] = useState("");
    const [userPassword, setUserPassword] = useState("");
    const [register, setRegister] = useState(false);
    const [showConfig, setShowConfig] = useState(false);


    async function validateName(userName) {
        const response = await userExists(userName);
        setSent(false)
        if (response.length === 0) {
            setNewUser(true);
            return false;
        }
        return true;
    }

    const validateUser = useCallback(async() => {
        const isValidated = await validateName(userName); 
        if (isValidated) {
            const response = await checkUser(userName, userPassword);
            if (response.length === 0) {
                setLogWithError(true);
                setSent(false);
            } else {
                setAuth(true);
                console.log(response)
                setDataUser({
                    "userName": response[0].userName,
                    "id": response[0].id,
                    "favorites": response[0].favorites 
                })
            }
        }
    }, [userName, userPassword, setAuth, setDataUser])
    
    const handleSubmit = (e) => {
        e.preventDefault();
        setSent(true);
        setNewUser(false);
        setLogWithError(false);
    }

    useEffect(() => {
        if (sent) {
            validateUser();
        }
    }, [sent, logWithError, validateUser]);

    useEffect(() => {
        if (newUser && userName === "") {
            setNewUser(false)
        }
        if (logWithError && userPassword === "") {
            setLogWithError(false)
        }
    }, [userName, userPassword, newUser, logWithError])

    return (
        auth 
            ?   <>
                    <Cuenta dataUser={dataUser} />
                    <button style={{ backgroundColor: "red" }} onClick={() => {
                        setShowConfig(!showConfig)
                    }}>Configuración</button>
                    {showConfig && <Config setAuth={setAuth} setDataUser={setDataUser} idUser={dataUser.id} />}
                    <button style={{ backgroundColor: "red" }} onClick={() => {
                        setAuth(false)
                        setDataUser({})
                    }
                    }>Salir</button>
                </>
            :   !register
                    ?   <>
                            <form className="loginForm">
                                Usuario
                                <input type="text" id="idUserName" onChange={
                                    (e) => {
                                        setUserName(e.target.value)
                                    }
                                    } />
                                {newUser && <span>No existe el usuario</span>}
                                <br />
                                Contraseña
                                <input type="password" id="idUserPassword" onChange={
                                    (e) => {
                                        setUserPassword(e.target.value)
                                    }
                                } />
                                <br/>
                                <input type="submit" onClick={handleSubmit} />
                                {logWithError && <span>Contraseña incorrecta</span>}
                            </form>
                            <button style={{
                                backgroundColor: "red"
                            }} onClick={() => setRegister(true)}>¿No tienes usuario? Crear uno</button>
                        </>
                    :   <>
                            <p>Registro</p>
                            <Registro setRegister={setRegister} />
                            
                            <button style={{
                                backgroundColor: "red"
                            }} onClick={() => setRegister(false)}>Volver al login</button>
                        </>
    );
}

export default Login;

/*

^^Pendiente^^:

-acción siguiente a un logueo correcto: definir estado global
-revisar comprobación de campos vacíos dinámicos

*/
