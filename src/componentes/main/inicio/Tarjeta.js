import tarjeta from "./tarjeta.module.css";

const Tarjeta = ({ datos }) => {
  return (
    <div className={tarjeta.fondo}>
      <img className={tarjeta.img} src={datos.image.url} />
      <p>{datos.name}</p>
    </div>
  );
};

export default Tarjeta;