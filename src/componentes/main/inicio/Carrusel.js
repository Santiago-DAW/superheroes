import React from "react";
import AliceCarousel from "react-alice-carousel";
import "react-alice-carousel/lib/scss/alice-carousel.scss";

const handleDragStart = (e) => e.preventDefault();

const Carrusel = ({data}) => {

  data = data.map(d => {
    return <img src={d.url} onDragStart={handleDragStart} role="presentation" alt=""/>
  })

  return (
    <>
    <br/>
    <br/>
    <br/>
    <br/>
    <div style={{ width: "80%", backgroundColor: "black" }}>
    <AliceCarousel items={data} />
    </div>
    </>
  );
  }

export default Carrusel;