import Tarjeta from "./Tarjeta";
import styles from "./lista.module.css";

const Lista = ({ lista }) => {
  return (
    <div className={styles.tarjetas}>
      {lista.map(item => {
        return <Tarjeta key={item.id} datos={item} />
      })}
    </div>
  );
};

export default Lista;