import { useState, useEffect } from "react";
import { get_datos_API_FAKE } from "../../../servicios/api_fake_server";
import { estructurar_heroes_villanos } from "../../../utilidades/util_personajes";
import Portadas from "./Portadas";
import Carrusel from "./Carrusel";
import "./inicio.css";

const Inicio = () => {
  // Estados - Inicio:
  const [heroesYVillanos, setHeroesYVillanos] = useState({})
  const URL_DATA = `http://localhost:4000/personajes`;

  
  const [personajesCarrusel, setPersonajesCarrusel] = useState([]);
const [loading, setLoading] = useState(true);

  useEffect(() => {
    get_datos_API_FAKE("personajes")
    .then((response) => {
      response.forEach(result => {
        setPersonajesCarrusel(personajesCarrusel => [...personajesCarrusel, result.image])
      })
    })
    .then(() => setPersonajesCarrusel(personajesCarrusel => personajesCarrusel.slice(111, 121)))
    .then(setLoading(false))
  }, [])

  useEffect(() => {
    get_datos_API_FAKE("personajes")
      .then(personajes => {
        return estructurar_heroes_villanos(personajes);
      })
      .then(setHeroesYVillanos);
  }, []);
  
  return (
    <main className="main_inicio">
      <div className="contenedor_inicio">
        {!loading && <Carrusel data={personajesCarrusel}/>}
        <Portadas />
      </div>
    </main>
  );
}

export default Inicio;