import React, { useCallback, useEffect, useState } from 'react'
import { NavLink } from 'react-router-dom';
import { nameDuplicated, newUser } from '../../../servicios/api_fake_server';
import "./Registro.css"

const Registro = ({ setRegister }) => {
    const [name, setName] = useState("");
    const [password, setPassword] = useState("");
    const [sent, setSent] = useState(false);
    const [creado, setCreado] = useState(false);
    const [nameGotten, setNameGotten] = useState(false);

    
    const nameAvailable = useCallback(async() => {
      const response = await nameDuplicated(name);
        if (response.length === 0) {
          setNameGotten(false)
          return false;
        }
        setNameGotten(true)
        return true;
    }, [name]);

    const createNewUser = useCallback(async() => {
      await newUser(name, password);
        setCreado(true)
    }, [name, password]);

    const handleSubmit = (e) => {
      e.preventDefault();
      setSent(true)
    }


    useEffect(() => {
      if (sent && nameAvailable()) {
        createNewUser()
      }
    }, [sent, createNewUser, nameAvailable]);
    
    useEffect(() => {
      nameAvailable()
    }, [nameAvailable])
    
    
    return (
      <>
    {
    creado ?
    
    <div>
      Usuario creado con éxito.
      <NavLink to="/login" onClick={(e) => {
        e.preventDefault()
        setRegister(false)
      }
      }>Entrar</NavLink>
    </div>

    :

    <form className='registerForm'>
      Nombre
        <input type="text" onChange={(e) => 
          setName(e.target.value)
        } />
          {
            nameGotten && <span>Nombre de usuario no disponible</span>
          }
        <br/>
        Contraseña
        <input type="password" onChange={(e) => setPassword(e.target.value)} />
        <br/>
        <input type="submit" onClick={handleSubmit} value="Registrarse" disabled={(nameGotten || name === "" || password === "") && true} />
    </form>
    }
    </>
  )
}

export default Registro;

/*
  Opcional (cuando sobre el tiempo):

  -comprobación de que la contraseña es coincidente en un segundo campo (semiopcional)


*/