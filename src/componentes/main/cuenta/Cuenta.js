import React from 'react'
import { NavLink } from 'react-router-dom';

const Cuenta = ({ dataUser }) => {

  return (
    <div style={{
      display: "flex",
      flexDirection: "column",
      alignItems: "center",
      textAlign: "center",
      padding: "10rem"
      
    }}>
      Te damos de nuevo la bienvenida {dataUser.userName}
      <ul>
        <li>
        <NavLink to="/favoritos">Favoritos</NavLink>
          </li>
      </ul>
      </div>
  )
}

export default Cuenta;