import React from 'react'
import { NavLink } from 'react-router-dom';
import { deleteUser } from '../../../servicios/api_fake_server';

const Config = ({ setAuth, setDataUser, idUser }) => {

  console.log(idUser)

  return (
    <>

      <button style={{ backgroundColor: "red" }}
      onClick={() => {
        deleteUser(idUser)
        setDataUser({})
        setAuth(false)
      }
      }
      >Borrar cuenta</button>
    
    </>
  )
}

export default Config