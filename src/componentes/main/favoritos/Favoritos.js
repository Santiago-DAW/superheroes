import { useState, useEffect } from "react";
import { NavLink, useOutletContext } from "react-router-dom";
import Lista from "../personajes/personajes-cmpnts/Lista";

const Favoritos = ({ favoritesState }) => {

  const [list, setList] = useState([]);
  const [loading, setLoading] = useState(true);
  const GET_FAVORITE = `http://localhost:4000/personajes`;
  const [loaded, setLoaded] = useState(false);
  useEffect(() => {
    if (typeof(favoritesState.favorites) == 'object' && !loaded) {
      fetch(
        favoritesState.favorites.map(async(favorite) => {
          let result = await fetch(`${GET_FAVORITE}/${favorite}`)
          .then(response => response.json())
          setList((list) => {
            return [...list, {...result, ...{favorite: true}}]
          })
        })
        )
        .then(() => {
          setLoading(false)
          setLoaded(true)
        }
        )
    }
  }, [GET_FAVORITE, favoritesState.favorites, loaded]);

  const [deletedFavorite, setDeletedFavorite] = useState();
  useEffect(() => {
    setList((list) => list.filter(element => element.id !== deletedFavorite))
  }, [deletedFavorite])




  return (
    <>
      <h2>Favoritos</h2>
      
      {
        favoritesState.favorites ?
        loading ?
        "Cargando..." :
        list.length === 0 ?
        <>
        <p>No tienes favoritos. Explora el catálogo de personajes que tenemos disponible en</p>
        <NavLink to="/personajes">Personajes</NavLink>
        </>
        :
        <div className="">

        <Lista personajes={list} favorites={favoritesState.favorites} setFavorites={favoritesState.setFavorites} setDeletedFavorite={setDeletedFavorite}/>
        
    
    </div> :
<>
        ¿Quieres entrar en tu cuenta?
        <NavLink to="/login">Entrar</NavLink>
</>
      }
    </>
  );
}

export default Favoritos;
