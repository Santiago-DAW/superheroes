import { useState, useEffect } from "react";
import { get_datos_API_FAKE } from "../../../servicios/api_fake_server";
import { estructurar_alfabeticamente } from "../../../utilidades/util_personajes";
import { extraer_claves_objeto } from "../../../utilidades/util_funciones";
import Filtros from "./personajes-cmpnts/Filtros";
import Alfabeto from "./personajes-cmpnts/Alfabeto";
import Lista from "./personajes-cmpnts/Lista";
import Paginacion from "./personajes-cmpnts/Paginacion";
import "./personajes.css";

const Personajes = ({ favoritesState }) => {
  // Estados - Personajes:
  const [personajes, setPersonajes] = useState([]);
  const [lista, setLista] = useState([]);
  // Estados - Filtros:
  const [propiedades, setPropiedades] = useState({});
  // Estados - Alfabeto:
  const [alfabeto, setAlfabeto] = useState([]);
  const [letra, setLetra] = useState('');
  // Estados - Paginacion:
  const [paginacion, setPaginacion] = useState({})
  const [paginaActual, setPaginaActual] = useState(0);

  useEffect(() => {
    get_datos_API_FAKE("personajes")
      .then(async personajes => {
        const datos = {};
        datos.ordenAlfabetico = await estructurar_alfabeticamente(personajes);
        datos.claves = await extraer_claves_objeto(datos.ordenAlfabetico);
        datos.ordenAlfabetico.length = datos.claves.length;
        datos.letra = datos.claves[0];
        return datos
      })
      .then(datos => {
        setPersonajes(datos.ordenAlfabetico);
        setAlfabeto(datos.claves);
        setLetra(datos.letra);
        return datos;
      })
      .then(datos => {
        const letra = datos.letra;
        const personajes = datos.ordenAlfabetico[letra];
        const lista = generar_lista(personajes, paginaActual);
        const paginacion = generar_obj_paginacion(personajes, letra, paginaActual);
        setLista(lista);
        setPaginacion(paginacion);
      });
  }, []);

  useEffect(() => {
    if (personajes.length) {
      if (paginacion.letra != letra) setPaginaActual(0);
      let nuevaLista = generar_lista(personajes[letra], paginaActual);
      const nuevaPaginacion = generar_obj_paginacion(personajes[letra], letra, paginaActual);
      if (favoritesState.favorites) {
        nuevaLista = nuevaLista.map(personaje => {
          const id = personaje.id;
          return favoritesState.favorites.includes(id)
                  ? {...personaje, favorite: true}
                  : {...personaje, favorite: false}
        })
      }
      setLista(nuevaLista);
      setPaginacion(nuevaPaginacion);
    }
  }, [personajes, letra, paginaActual, favoritesState.favorites]);
  
  const generar_obj_paginacion = (personajes, letra, pagActual, personajesPorPag = 16) => {
    const numPersonajes = personajes.length;
    const numPaginas = Math.ceil(numPersonajes / personajesPorPag);
    return {letra, numPaginas, pagActual}
  }

  const generar_lista = (personajes, pagActual, personajesPorPag = 16) => {
    const lista = [];
    const numPersonajes = personajes.length;
    const inicio = pagActual * personajesPorPag;
    const fin = inicio + personajesPorPag - 1;
    for (let i = inicio; i <= fin && i < numPersonajes; i++) {
      lista.push(personajes[i]);
    }
    return lista;
  };

  return (
    <main className="main">
      <Filtros />
      <Alfabeto alfabeto={alfabeto} letra={letra} setLetra={setLetra} />
      <Lista personajes={lista} favorites={favoritesState.favorites} setFavorites={favoritesState.setFavorites}/>
      <Paginacion paginacion={paginacion} setPaginaActual={setPaginaActual} />
    </main>
  );
};

export default Personajes;
