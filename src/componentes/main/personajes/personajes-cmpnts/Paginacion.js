import "./paginacion.sass";

const Paginacion = ({ paginacion, setPaginaActual }) => {
  const {numPaginas, pagActual} = paginacion;
  const indices = Array(numPaginas).fill(1).map((num, i) => num + i);

  return (
    <div className="contenedor_paginacion">
      {pagActual != 0
        ? <button className="boton-pagina anterior" onClick={() => setPaginaActual(pagActual-1)}>Anterior</button> 
        : numPaginas > 1 ? <button className="boton-pagina anterior desactivado" disabled>Anterior</button> : ""
      }
      {numPaginas > 1 && indices.map(indice => {
        let clase = "boton-pagina";
        if (indice == pagActual + 1) clase = clase + " pagina-seleccionada";
        return <button key={`pag${indice}`} className={clase} onClick={() => setPaginaActual(indice - 1)}>{indice}</button>
      })}
      {pagActual != numPaginas-1 
        ? <button className="boton-pagina siguiente" onClick={() => setPaginaActual(pagActual+1)}>Siguiente</button> 
        : numPaginas > 1 ? <button className="boton-pagina siguiente desactivado" disabled>Siguiente</button> : ""
      }
    </div>
  );
};

export default Paginacion;