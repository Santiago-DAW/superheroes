import { useState } from "react";
import InputCheck from "./filtros-cmpnts/InputCheck";
import InputRadio from "./filtros-cmpnts/InputRadio";
import InputRange from "./filtros-cmpnts/InputRange";
import InputNumber from "./filtros-cmpnts/InputNumber";

const listaChecks = {
  intelligence: "inteligencia",
  strength: "fortaleza",
  speed: "velocidad",
  durability: "durabilidad",
  power: "poder",
  combat: "combate",
}
const arrayChecks = Object.entries(listaChecks);

// const extraer_datos_formulario = (e) => Object.fromEntries(Array.from(new FormData(e.target)));

const Filtros = ({ setDatos }) => {
  const [datosForm, setDatosForm] = useState({gender: 'all'});
  const [checks, setChecks] = useState({});

  const handleSubmit = (e) => {
    e.preventDefault();
    const datos = {...datosForm};
    for (const [k, v] of Object.entries(checks)) {
      if (v === false) {
        delete datos[k];
      }
    }
    console.log("Elevacion datos: ", datos);
    setDatos(datos);
  }
  console.log(checks, datosForm);

  const handleReset = () => {
    setDatosForm({sexo: 'all'});
    setChecks({});
  }

  const handleChange = (fn) => {
    return ({ target : t }) => fn(data => ({...data, [t.name]: t.type === 'checkbox' ? t.checked : t.value}));
  };

  const handleChangeDatos = handleChange(setDatosForm);
  
  const handleChangeChecks = ({ target: t }) => {
    setChecks(cks => ({...cks, [t.name]: t.checked}));
    return !datosForm[t.name] ? setDatosForm(dtf => ({...dtf, [t.name]: 50})) : null;
  };
  
  return (
    <div className="contenedor_filtros">
      <form onSubmit={handleSubmit}>
        {/* Select */}
        <label htmlFor="vista">Tipo de vista</label>
        <select name="select" id="vista" onChange={handleChange}>
          <option value="alfabetica">Alfabetica</option>
          <option value="buenos">Buenos</option>
          <option value="malos">Malos</option>
          <option value="otros">Otros</option>
        </select>

        {/* Sexo */}
        <fieldset>
          <legend>Genero:</legend>
          <InputRadio name={"gender"} value={"all"} text={"todos"} data={datosForm} setRadio={handleChangeDatos} />
          <InputRadio name={"gender"} value={"male"} text={"hombre"} data={datosForm} setRadio={handleChangeDatos} />
          <InputRadio name={"gender"} value={"female"} text={"mujer"} data={datosForm} setRadio={handleChangeDatos} />
        </fieldset>

        <fieldset>
          <legend>Poderes:</legend>
          {arrayChecks.map(opciones => {
            const [value, text] = opciones;
            return (
              <div key={value}>
                <InputCheck value={value} text={text} checks={checks} setChecked={handleChangeChecks} />
                { checks[value] 
                    ? <>
                        <InputRange name={value} value={datosForm[value]} setRange={handleChangeDatos} />
                        <InputNumber value={value} data={datosForm} setNumber={handleChangeDatos} />
                      </>
                    : null 
                }
                <hr />
              </div>
            );
          })}
        </fieldset>

        <button type="submit">Buscar</button>
        <button type="reset" onClick={handleReset}>Restablecer</button>
      </form>
    </div>
  );
}

export default Filtros;