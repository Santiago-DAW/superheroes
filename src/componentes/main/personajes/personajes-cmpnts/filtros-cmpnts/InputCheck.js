const InputCheck = ({ value, text, checks, setChecked }) => {
  if (!text) text = value;

  return (
    <>
      <input type="checkbox" id={`check-${value}`} name={value} checked={checks.value} onChange={setChecked} />
      <label htmlFor={`check-${value}`}> {text.charAt(0).toUpperCase() + text.slice(1)}</label>
      <br />
    </>
  );
};

export default InputCheck;
