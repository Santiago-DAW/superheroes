const InputRange = ({ name, value, setRange }) => {
  return (
    <>
      <input type="range" name={name} min="0" max="100" step="1" value={value}  onChange={setRange} />
    </>
  );
}

export default InputRange;
