const InputRadio = ({ name, value, text = null, data, setRadio }) => {
  if (text === null) text = value;

  return (
    <>
      <input type="radio" name={name} id={value} value={value} checked={data[name] == value} onChange={setRadio} />
      <label htmlFor={value}> {text.charAt(0).toUpperCase() + text.slice(1)}</label>
      <br />
    </>
  );
}

export default InputRadio;