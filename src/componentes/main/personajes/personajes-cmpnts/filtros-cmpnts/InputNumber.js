const InputNumber = ({ value, data, setNumber }) => {
  return (
    <input type="number" id={value} name={value} value={data[value]} min="0" max="100" onClick={setNumber} onChange={setNumber} />
  );
}

export default InputNumber;
