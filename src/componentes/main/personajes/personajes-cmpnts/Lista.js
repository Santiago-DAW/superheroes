import { useEffect, useState } from "react";
import { addFavorite } from "../../../../servicios/api_fake_server";
import Tarjeta from "./Tarjeta";

const Lista = ({ personajes, favorites, setFavorites, setDeletedFavorite }) => {
  const [newFavorite, setNewFavorite] = useState();

  const id = JSON.parse(localStorage.getItem('dataUser')).id;

  useEffect(() => {
    if (newFavorite) {
      if (favorites.includes(newFavorite)) {
        addFavorite(id, favorites.filter(favorite => favorite != newFavorite))
        setFavorites((favorites) => favorites.filter(favorite => favorite != newFavorite))
      } else {
        addFavorite(id, [...favorites, newFavorite].sort((a, b) => a - b))
        setFavorites((favorites) => [...favorites, newFavorite])
      }
      setNewFavorite("")
    }
  }, [id, newFavorite, setFavorites])

  return (
    <div className="contenedor_personajes">
        {personajes.map(item => {
          return <Tarjeta key={item.id} datos={item} setNewFavorite={setNewFavorite} setDeletedFavorite={setDeletedFavorite}/>
        })}
    </div>
  );
};

export default Lista;