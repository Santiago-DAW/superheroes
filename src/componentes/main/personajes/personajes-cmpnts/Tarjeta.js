

const Tarjeta = ({ datos, setNewFavorite, favorite, setDeletedFavorite }) => {

  const auth = JSON.parse(localStorage.getItem('auth'));

  return (
    <div className="contenedor_tarjeta">
      <div className="contenedor_imagen">
        <img className="img" src={datos.image.url} alt="" />
      </div>
      <div className="contenedor_datos-tarjeta">
        <p className="nombre_personaje">{datos.name}</p>
        {auth &&
          <div className="contenedor_botones">
            {datos.favorite || favorite
              ? <button className="boton_eliminar" onClick={() => {
                setNewFavorite(datos.id)
                setDeletedFavorite(datos.id)
              }
              }>eliminar favoritos</button>
              : <button className="boton_favoritos" onClick={() => setNewFavorite(datos.id)}>añadir favoritos</button>
            }          
          </div>
        }
      </div>
    </div>
  );
};

export default Tarjeta;