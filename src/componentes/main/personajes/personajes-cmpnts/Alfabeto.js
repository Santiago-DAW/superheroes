const Alfabeto = ({ alfabeto, letra: LETRA, setLetra }) => {
  const prueba = (e) => {
    e.preventDefault();
    const letra = e.target.id;
    return setLetra(letra);
  }

  return (
      <div className="contenedor_alfabeto">
        {alfabeto.map(letra => {
          let clase_active = "";
          if (letra == LETRA) clase_active = "active";
          return <button className={`boton_alfabeto ${clase_active}`} key={letra} id={letra} onClick={prueba}>{letra}</button>
        })}
      </div>
    );
};

export default Alfabeto;