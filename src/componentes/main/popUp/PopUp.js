import { useEffect, useState } from "react";
import Imagen from "./popUp-cmpnts/Imagen";
import Nombre from "./popUp-cmpnts/Nombre";
import Pestañas from "./popUp-cmpnts/Pestañas";
import Datos from "./popUp-cmpnts/Datos";
import "./popUp.sass";

const establecer_fondo = (image) => {
  const fondo = document.body.querySelector(".contenedor_datos-personaje");
  fondo.style.background = `linear-gradient(rgba(255, 255, 255, 0.6), rgba(255, 255, 255, 0.5)), url(${image}) no-repeat center center fixed`;
  fondo.style.backgroundSize = "cover";
}

const PopUp = ({ personaje, setPopUpVisible }) => {
  const [datos, setDatos] = useState({});
  const [pestañas, setPestañas] = useState([]);
  const [seleccionado, setSeleccionado] = useState('powerstats');
  const [expediente, setExpediente] = useState({});

  const styles = {
    background: `linear-gradient(rgba(255, 255, 255, 0.6), rgba(255, 255, 255, 0.5)), url(${datos.image})`,
    backgroundRepeat: "no-repeat",
    backgroundSize: "cover",
  }

  useEffect(() => {
    const {id, name, image: {url: image}, ...expediente} = personaje;
    setDatos({id, name, image});
    setPestañas(Object.keys(expediente));
    setExpediente(expediente);
    establecer_fondo(image);
  }, [personaje]);

  return (
    <div className="contenedor_popUp">
      <button style={{backgroundColor: "red"}} onClick={() => {
        setPopUpVisible(false)
      }}>Cerrar</button>
      {/* <div className="contenedor_datos-personaje" style={{background: `linear-gradient(rgba(255, 255, 255, 0.6), rgba(255, 255, 255, 0.5)), url(${datos.image})`}}> */}
      <div className="contenedor_datos-personaje">
        <Imagen datos={datos} />
        <div className="contenedor_detalles">
          <Nombre nombre={datos.name} />
          <Pestañas pestañas={pestañas} seleccionado={seleccionado} setSeleccionado={setSeleccionado} />
          <Datos datos={expediente} seleccionado={seleccionado} />
        </div>
      </div>
    </div>
  );
}

export default PopUp;
