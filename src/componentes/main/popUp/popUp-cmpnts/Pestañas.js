const Pestañas = ({ pestañas, seleccionado, setSeleccionado }) => {
  const listaPestañas = ["powerstats", "biography", "appearance", "connections"];

  const handleSeleccionado = (e) => {
    setSeleccionado(e.target.textContent);
  }

  return (
    <div className="contenedor_pestañas">
      {pestañas.filter(pestaña => listaPestañas.includes(pestaña)).map((pestaña, i) => {
        let activa = "";
        if (seleccionado == listaPestañas[i]) activa = "pestaña-activa";
        return <button key={i} className={`pestaña ${activa}`} onClick={handleSeleccionado}>{pestaña}</button>;
      })}
    </div>
  );
}

export default Pestañas;
