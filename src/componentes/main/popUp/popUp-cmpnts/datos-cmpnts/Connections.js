const Connections = ({ connections }) => {
  const conexiones = connections ? Object.entries(connections) : [];

  return (
    <ul className="datos connections seleccionado">
      {conexiones.map(item => {
        const [clave, valor] = item;
        return (
          <li key={clave}>
            <span>{clave}</span>
            <span>{valor}</span>
          </li>
        );
      })}
    </ul>
  );
}

export default Connections;
