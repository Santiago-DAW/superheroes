const Powerstats = ({ powerstats }) => {
  const estadisticas = powerstats ? Object.entries(powerstats) : [];

  return (
    <ul className="datos powerstats seleccionado">
      {estadisticas.map(powerstat => {
        const [clave, valor] = powerstat;
        return (
          <li key={clave}>
            <div>
              <i className="fa-solid fa-shield-halved"></i>
              <span>{clave}</span>
            </div>
            <span>{valor}</span>
          </li>
        );
      })}
    </ul>
  );
};

export default Powerstats;