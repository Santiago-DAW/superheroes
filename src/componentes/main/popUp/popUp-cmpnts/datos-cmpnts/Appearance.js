const Appearance = ({ appearance }) => {
  const apariencia = appearance ? Object.entries(appearance) : [];

  return (
    <ul className="datos appearance seleccionado">
      {apariencia.map(item => {
        const [clave, valor] = item;
        return (
          <li key={clave}>
            <span>
              <i className="fas fa-star"></i> {clave}
            </span>
            <span>{Array.isArray(valor) ? valor[1] : valor}</span>
          </li>
        );
      })}
    </ul>
  );
}

export default Appearance;