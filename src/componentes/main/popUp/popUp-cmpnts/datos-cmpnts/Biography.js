const Biography = ({ biography }) => {
  const biografia = biography ? Object.entries(biography) : [];

  return (
    <ul className="datos biography seleccionado">
      {biografia.map(item => {
        const [clave, valor] = item;
        return (
          <li key={clave}>
            <span>{clave}</span>
            <span>
              {Array.isArray(valor) 
                ? valor.join(', ') + "." 
                : valor
              }
            </span>
          </li>
        );
      })}
    </ul>
  );
}

export default Biography;