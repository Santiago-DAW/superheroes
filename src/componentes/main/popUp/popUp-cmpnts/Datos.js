import Powerstats from "./datos-cmpnts/Powerstats";
import Biography from "./datos-cmpnts/Biography";
import Appearance from "./datos-cmpnts/Appearance";
import Connections from "./datos-cmpnts/Connections";

const Datos = ({ datos, seleccionado }) => {
  return (
    <div className="contenedor_datos">
      {
        seleccionado == "powerstats" ? <Powerstats powerstats={datos.powerstats} /> : "" ||
        seleccionado == "biography" ? <Biography biography={datos.biography} /> : ""  ||
        seleccionado == "appearance" ? <Appearance appearance={datos.appearance} /> : "" ||
        seleccionado == "connections" ? <Connections connections={datos.connections} /> : ""
      }
    </div>
  );
}

export default Datos;
