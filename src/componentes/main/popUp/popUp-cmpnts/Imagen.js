const Imagen = ({ datos }) => {
  // console.log("Imagen-reseñas: ", reseñas);

  return (
    <div className="contenedor_imagen-personaje">
      <img src={datos.image} alt={`"Imagen de ${datos.name}"`} />
    </div>
  );
}

export default Imagen;