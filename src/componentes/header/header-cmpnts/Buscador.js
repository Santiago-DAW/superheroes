import { useState, useEffect } from "react";
import { busqueda_nombre_API } from "../../../servicios/api_super_hero";
import Resultados from "./buscador-cmpnts/Resultados";

const Buscador = ({ setPopUp }) => {
  const [busqueda, setBusqueda] = useState('superman');
  const [resultados, setResultados] = useState([]);

  useEffect(() => {
      const timeoutId = setTimeout(() => {
        busqueda_nombre_API(busqueda)
          .then(result => {
            const {response, results} = result;
            if (response == 'error') return [];
            return results;
          })
          .then(results => {
            setResultados(results);
          });
      }, 500);
      return function () {
        clearTimeout(timeoutId);
      }
  }, [busqueda]);

  const handleBusqueda = (e) => {
    e.preventDefault();
    setBusqueda(e.target.value);
  };

  return (
    <div className="contenedor_buscador">
      <label htmlFor="buscador">Search: </label>
      <input id="buscador" type="text" defaultValue={busqueda} onChange={handleBusqueda} />
      {resultados.length == 0 
        ? "No hay resultados" 
        : <Resultados personajes={resultados} setPopUp={setPopUp} />
      }
    </div>
  );
}

export default Buscador;