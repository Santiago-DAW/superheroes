const Resultados = ({ personajes, setPopUp }) => {

  const handlePersonajeClicado = (e) => {
    e.preventDefault();
    const id = e.target.closest("li.resultado_personaje").id;
    const personaje = personajes.find(personaje => personaje.id === id);
    setPopUp(personaje);
  }

  return (
    <ul className="resultados_lista-busqueda">
      {personajes.map(personaje => {
        return (
          <li key={personaje.id} className="resultado_personaje" id={personaje.id}>
            <a href="#" onClick={handlePersonajeClicado}>
              <div className="resultado_imagen-personaje">
                <img src={personaje.image.url} alt={personaje.name} />
              </div>
              <span className="resultado_nombre-personaje">{personaje.name}</span>
            </a>
          </li>
        );
      })}
    </ul>
  );
}

export default Resultados;