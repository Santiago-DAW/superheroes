import { useEffect, useState } from "react";
import Login from "./usuario-cmpnts/Login";
import Registro from "./usuario-cmpnts/Registro";
import Cuenta from "./usuario-cmpnts/Cuenta";
import "./usuario.sass";

const Usuario = () => {
  const [usuarioVisible, setUsuarioVisible] = useState(false);
  const [claseUsuario, setClaseUsuario] = useState("");

  useEffect(() => {
    if (usuarioVisible) setClaseUsuario("popUp-usuario-acciones");
    else setClaseUsuario("");
  }, [usuarioVisible]);

  return (
    <>
      <div className="contenedor_usuario-icono" onClick={() => setUsuarioVisible(v => !v)}>
        {"<icono usuario>"}
      </div>
      <div className={`contenedor_popUp-usuario ${claseUsuario}`} onClick={() => setUsuarioVisible(v => !v)}>
        {usuarioVisible
          ? <div className="contenedor_usuario-acciones">
              <Login />
              <Registro />
              <Cuenta />
            </div>
          : ""
        }
      </div>
    </>
  );
}

export default Usuario;