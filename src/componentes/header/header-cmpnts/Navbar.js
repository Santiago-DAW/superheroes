import { NavLink } from "react-router-dom";

const seleccionado = (e) => {
  console.log(e.target.textContent);
  console.log(e.target);
}

const Navbar = () => {
  return (
    <nav className="menu_navegacion">
      <ul>
        <li>
          <NavLink to="login" onClick={seleccionado}>Mi cuenta</NavLink>
        </li>
        <li>
          <NavLink to="" onClick={seleccionado}>inicio</NavLink>
        </li>
        <li>
          <NavLink to="personajes" onClick={seleccionado}>personajes</NavLink>
        </li>
        <li>
          <NavLink 
          to="favoritos" onClick={seleccionado}>favoritos</NavLink>
        </li>
      </ul>
    </nav>
  );
}

export default Navbar;