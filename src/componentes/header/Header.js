import { useState } from "react";
import Creditos from "./header-cmpnts/Creditos";
import Usuario from "./header-cmpnts/Usuario";
import Titulo from "./header-cmpnts/Titulo";
import Navbar from "./header-cmpnts/Navbar";
import Buscador from "./header-cmpnts/Buscador";
import "./header.sass";

const Header = ({ setPopUp }) => {
  const [buscar, setBuscar] = useState(false);

  return (
    <header className="contenedor_header">
      <Creditos />
      <Usuario />
      <Titulo />
      <Navbar />
      <button onClick={() => setBuscar(!buscar)} style={{backgroundColor: "red", width: "5rem", height: "3rem", fontSize: "100%"}} >Buscar</button>
      {buscar && <Buscador setPopUp={setPopUp} />}
    </header>
  );
}

export default Header;