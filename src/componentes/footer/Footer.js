import "./footer.scss";

const Footer = () => {
  return (
    <footer>
      <div className="contenedor_titulo">superheroes</div>
      <div className="contendor_redes-sociales">
        <i>facebook</i>
        <i>instagram</i>
        <i>twiter</i>
        <i>youtube</i>
      </div>
      <div className="contenedor_autores">
        <p>Autores:</p>
        <ul>
          <li>Ricardo Silva</li>
          <li>Santiago Goyes</li>
        </ul>
      </div>
    </footer>
  );
}

export default Footer;