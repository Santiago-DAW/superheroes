// Devuelve un array con las claves del objeto. :Promise - Array de claves del objeto
export const extraer_claves_objeto = async (objeto) => {
  const claves = Object.keys(await objeto);
  return claves;
}