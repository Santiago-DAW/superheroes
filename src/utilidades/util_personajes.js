// ---- Utilidades (funciones) - Personajes ----
// Descripcion: contiene funciones para manipular la informacion recibida de los personajes.

// Devuelve un pequeño listado de los personajes. :Promise - Array objetos personaje.
import { busqueda_id_API } from "../servicios/api_super_hero";
const lista_parcial_personajes = async (id_min = 1, id_max = 20) => {
  const personajes = [];
  for (let id = id_min; id <= id_max; id++) {
    const personaje = await busqueda_id_API(id);
    personajes.push(personaje);
  }
  return Promise.all(personajes);
};

// Devuelve una array o un objeto con claves alfabeticas. :Array[claves][Objetos-personaje] || :Objeto{claves:{ids:Objeto-pesonaje}}
const estructurar_alfabeticamente = async (lista_personajes, salida_objeto = false) => {
  if (salida_objeto)
    return estructurar_alfabeticamente_objeto(lista_personajes);
  else
    return estructurar_alfabeticamente_array(lista_personajes);
};

// Devuelve un array de arrays con los personajes organizados por claves alfabeticas. :Array[claves][Objetos-personaje]
const estructurar_alfabeticamente_array = async (lista_personajes) => {
  const array_personajes = await lista_personajes;
  const estructura_array = array_personajes.reduce((acc, personaje) => {
    const inicial = personaje.name[0].toLowerCase();
    if (!acc[inicial]) {
      acc[inicial] = [];
    }
    acc[inicial].push(personaje);   // acc[inicial] = [...acc[inicial], personaje];  
    return acc;
  }, []);
  return estructura_array;
};

// Devuelve un objetos con claves alfabeticas, cada clave tiene un array de objetos personaje. :Objeto{claves:{ids:Objeto-pesonaje}}
const estructurar_alfabeticamente_objeto = async (lista_personajes) => {
  const array_personajes = await lista_personajes;
  const estructura_objeto = array_personajes.reduce((acc, personaje) => {
    const inicial = personaje.name[0].toLowerCase();
    const id = personaje.id;
    if (!acc[inicial]) {
      acc[inicial] = {};
    }
    acc[inicial][id] = personaje;
    return acc;
  }, {});
  return estructura_objeto;
};

const estructurar_heroes_villanos = async (lista_personajes) => {
  const array_personajes = await lista_personajes;
  const vinculacion = {heroes: [], villanos: [], otros: []};
  return array_personajes.reduce((acc, personaje) => {
    const vinculacion = personaje.biography.alignment;
    if (vinculacion == 'good') acc.heroes.push(personaje);
    else if (vinculacion == 'bad') acc.villanos.push(personaje);
    else acc.otros.push(personaje);
    return acc;
  }, vinculacion);
};

export { lista_parcial_personajes, estructurar_alfabeticamente, estructurar_heroes_villanos };