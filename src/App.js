import { useEffect, useState } from "react";
import Header from "./componentes/header/Header";
import Inicio from "./componentes/main/inicio/Inicio";
import PopUp from "./componentes/main/popUp/PopUp";
import Personajes from "./componentes/main/personajes/Personajes";
import Favoritos from "./componentes/main/favoritos/Favoritos";
import Error from "./componentes/main/error/Error";
import Footer from "./componentes/footer/Footer";
import "./App.css";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import Login from "./componentes/main/login/Login";

function App() {
  // Estados - PopUp (trabaja con componente "Buscador"):
  const [popUp, setPopUp] = useState({});
  const [popUpVisible, setPopUpVisible] = useState(true);
  // Estados - Login:
  const [auth, setAuth] = useState(false);
  const [dataUser, setDataUser] = useState({});
  const [favorites, setFavorites] = useState([]);

  const handlePopUpPersonaje = (datosPersonaje) => {
    setPopUp(datosPersonaje);
    setPopUpVisible(true)
  }

  useEffect(() => {
    setAuth(JSON.parse(window.localStorage.getItem('auth')));
    setDataUser(JSON.parse(window.localStorage.getItem('dataUser')));
  }, []);

  useEffect(() => {
    window.localStorage.setItem('auth', auth);
  }, [auth]);

  useEffect(() => {
    window.localStorage.setItem('dataUser', JSON.stringify(dataUser));
    setFavorites(() => JSON.parse(window.localStorage.getItem('dataUser')).favorites)
  }, [dataUser, setFavorites]);

  useEffect(() => {
    if (auth && favorites) {
      if (favorites.length !== JSON.parse(window.localStorage.getItem('dataUser')).favorites.length) {
        let newDataUser = JSON.parse(window.localStorage.getItem('dataUser'));
        newDataUser = {
          "favorites": favorites,
          "id": dataUser.id,
          "userName": dataUser.userName
        }
        window.localStorage.setItem('dataUser', JSON.stringify(newDataUser));
      }}
  }, [dataUser.id, dataUser.userName, favorites, auth])

  return (
    <>
      <BrowserRouter>
        <Header setPopUp={handlePopUpPersonaje} />
        {(Object.keys(popUp).length > 0 && popUpVisible) && <PopUp personaje={popUp} setPopUpVisible={setPopUpVisible} /> }
        <Routes>
          <Route path="/" >
          <Route path="login" element={ <Login auth={auth} setAuth={setAuth} dataUser={dataUser} setDataUser={setDataUser} /> } />
            <Route index element={ <Inicio /> } />
            <Route path="personajes" element={ <Personajes favoritesState={auth && {favorites, setFavorites}} setPopUp={handlePopUpPersonaje} /> } />
            <Route path="favoritos" element={ <Favoritos favoritesState={auth && {favorites, setFavorites}} /> } />
            <Route path="*" element={ <Error /> } />
          </Route>
        </Routes>
      </BrowserRouter>
      <Footer />
    </>
  );
}

export default App;
